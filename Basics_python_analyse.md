# Technology
- Sklearn
- Pandas
- Seaborn
- Matplotlib
# Visualisation

```py
df.describe()
df = pd.DataFrame(data)
df.columns = data.feature_names
df['PRICE'] = data.target
df.head()
df.info()  
df.isnull().sum()
df.corr()

df.dtypes.value_counts()
sns.heatmap(df.isna())
missing_value = df.isna().sum() / df.shape[0]
missing_value.sort_values()

def distributions(row, colum, data): 
    fig, ax = plt.subplots(nrows=row, ncols=colum, figsize=(20,16))
    col = data.columns
    index = 0
    for i in range(row):
        for j in range(colum):
            sns.histplot(data=data, x=col[index],ax=ax[i][j])
            index += 1
    # plt.tight_layout() 

distributions(7,2,df.select_dtypes('float'))

def category_count(dataset):
    for col in dataset.select_dtypes('object'):
        print(f'{col :-<50}{dataset[col].unique()}')

def category_pie(dataset):
    for col in dataset.select_dtypes('object'):
        plt.figure()
        dataset[col].value_counts().plot.pie()

def category_heatmap_y(dataset):
	for col in viral_coulmns:
    	plt.figure()
    	sns.heatmap(pd.crosstab(covid['Y_name'], covid[col]), annot=True, fmt='d')

# Category
{col:len(data[col].unique()) for col in data[str_cols].columns}

for col in blood_columns:
    plt.figure()
    sns.distplot(postive_covid[col], label='positive')
    sns.distplot(negative_covid[col], label='negative')
    plt.legend()

pd.crosstab(covid['SARS-Cov-2 exam result'], covid['Patient addmited to semi-intensive unit (1=yes, 0=no)'])

```
# MODEL CREATION and Preprocessing 

## Impute transformers for missing value imputation
- SimpleImputer
- KNNImputer
- IterativeImputer
- MissingIndicator

## Preprocessing
- Encodage
- Normalization
- Polynomial 
- Linear transform
- Discrétisation 

## Pipeline 
```
model = make_pipeline(PolynomialFeatures(), StandardScaler(), SGDClassifier(random_state=0))
```
- Pipeline pour certaine column
```
make_column_transformer 
make_column_selector
make_union
```

## Feature selection

Statistics test

Méthode :
- fit, fit_transform, get_params, get_support, inverse_transform, set_params, transform

- SelectKBest(chi2)
- chi2 
- SelectFromModel
- VarianceThreshold

## Unsupervised learning
- Isolation forst (anomli detection)
- KMeans (clustering) 
- PCA (visualization in lower dimension or dimension reduction)

## Ensemble learning
### Bagging, Boosting et stacking

- Bagging when overfitt (BaggingClassifier, RandomForest)
- Boosting when underfitting (AdaBoost, GrandientBoosting)
- Stacking when lot's of different models to stacks but slower (Stacking classifier)

## Metrics

```
print(confusion_matrix(y_test, y_pred))
print(classification_report(y_test, y_pred))

N, train_score, val_score = learning_curve(model, X_train, y_train, cv=4, 
scoring='f1', train_sizes=np.linspace(0.1,1,10))
print(val_score.mean())

plt.figure()
plt.plot(N, train_score.mean(axis=1), label='train')
plt.plot(N, val_score.mean(axis=1), label='test')
```
Cross validation :
- k-fold 
- shuffle split
- stratified kfold (choix de base sur)
- Group kfold (Qaund les données dépendent d'un group)


## Custom metrics
- For creating of custom scores

## Model Selection
- Grid search cv or randomsearch cv for looking best model's parameters

## Regression linéaire 
```
print('MSE', mean_squared_error(y, y_pred))
print('RMSE', np.sqrt(mean_squared_error(y, y_pred)))
print('MEDIANE ABSOLUTE ERROR', median_absolute_error(y, y_pred))
```

# HOW TO WORK (Basic approach)

Exploratory Data Analysis

TODO : Understand data for defined a modelisation's strategy 

I - Shape analysis:

	§ Find the target
	§ Shap of the dataset 
	§ Find missing values 
	§ Type of variables

II - Data Analyse :

	§ Visualisation of the target (histogramme/boxplot)
	§ Understand  all variables (seach)
	§ Visualisation of relations : features/target
	§ Find outliers

	Link between variables
		-- Them-self 
		-- Correlated features in general don't improve models
		-- Target variables
		-- Variables with their own categories 
		-- NaN analyse
		-- Hypothesis testing

	Always tried to do features engineering

	Target relation and category variables then target relation and continuous variables


Pre-processing

	§ Creation of Train Set / Test Set
	§ Delete NaN or Imputation
	§ Encodage
	§ Delete outliers harmful to the model
	§ Feature selection
	§ Feature engineering 
	§ Feature scaling

Modelling

	§ Evaluation function
	§ Trainning of different models
	§ Optimization with GridSearchCV
	§ Analysis of errors and repeat previous steps


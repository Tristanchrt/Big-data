PYTHON : 

https://cheatography.com/davechild/cheat-sheets/python/
https://perso.limsi.fr/pointal/_media/python:cours:mementopython3-english.pdf

Numpy :
http://datacamp-community-prod.s3.amazonaws.com/ba1fe95a-8b70-4d2f-95b0-bc954e9071b0

Pandas :
https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf
https://www.dataquest.io/blog/pandas-cheat-sheet/

Matplotlib :

http://datacamp-community-prod.s3.amazonaws.com/e1a8f39d-71ad-4d13-9a6b-618fe1b8c9e9

Seaborn : 

https://martinnormark.com/a-simple-cheat-sheet-for-seaborn-data-visualization-2/
https://s3.amazonaws.com/assets.datacamp.com/blog_assets/Python_Seaborn_Cheat_Sheet.pdf

Maths and Statistics : 

Probability : https://static1.squarespace.com/static/54bf3241e4b0f0d81bf7ff36/t/55e9494fe4b011aed10e48e5/1441352015658/probability_cheatsheet.pdf
Statistics : http://web.mit.edu/~csvoss/Public/usabo/stats_handout.pdf
Linear Algebra : https://minireference.com/static/tutorials/linear_algebra_in_4_pages.pdf
Matrices : https://fr.scribd.com/doc/7877744/Matrices-Cheat-Sheet
Calculus/Trignometry_etc : https://tutorial.math.lamar.edu/Extras/CheatSheets_Tables.aspx#CalcSheet
Stats_for_interviews : https://www.stratascratch.com/blog/a-comprehensive-statistics-cheat-sheet-for-data-science-interviews/

Machine learning :

https://stanford.edu/~shervine/teaching/cs-229/cheatsheet-machine-learning-tips-and-tricks

Deep learning : 

https://storage.googleapis.com/kaggle-forum-message-attachments/730211/14820/DeepLearning_AndrewNG.pdf
https://stanford.edu/~shervine/teaching/cs-230/

Scipy : 

http://datacamp-community-prod.s3.amazonaws.com/b5685b85-c4de-4987-926d-b999d0f5a8b6

Sklearn : 

https://s3.amazonaws.com/assets.datacamp.com/blog_assets/Scikit_Learn_Cheat_Sheet_Python.pdf
https://cheatography.com/sati/cheat-sheets/scikit-learn-cyber-security/
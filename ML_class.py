import numpy as np
from sklearn.datasets import make_regression
import matplotlib.pyplot as plt

np.random.seed(0) 
x, y = make_regression(n_samples=100, n_features=2, noise=10)

class LinearModelMultiClass():

    def __init__(self, x, y, learning_rate, n_iterations):
        self.__x = x
        self.__y = y
        self.__theta = None
        self.__X = None
        self.__learning_rate = learning_rate
        self.__n_iterations = n_iterations

    def init_model(self):
        self.__y = self.__y.reshape(self.__y.shape[0], 1)
        X = np.hstack((self.__x, np.ones((self.__x.shape[0], 1))))
        X = np.hstack((self.__x**2, X))
        np.random.seed(0)
        theta = np.random.randn(X.shape[1], 1)
        self.__X = X
        self.__theta = theta

    def get_theta(self):
        return self.__theta
    
    def get_X(self):
        return self.__X

    def model(self):
        return self.__X.dot(self.__theta)

    def predictions(self, X, theta):
        return X.dot(theta)

    def cost_function(self):
        m = len(self.__y)
        return 1/(2*m) * np.sum((self.model() - self.__y)**2)

    def grad(self):
        m = len(y)
        return 1/m * self.__X.T.dot(self.model() - self.__y)

    def gradient_descent(self):
        cost_history = np.zeros(self.__n_iterations)
        for i in range(0, self.__n_iterations):
            self.__theta = self.__theta - self.__learning_rate * self.grad() 
            cost_history[i] = self.cost_function() 
        return self.__theta, cost_history

    def coef_determination(self, pred):
        u = ((self.__y - pred)**2).sum()
        v = ((self.__y - self.__y.mean())**2).sum()
        return 1 - u/v

    def evaluation(self, pred):
        plt.figure()
        plt.scatter(self.__x[:,0], self.__y)
        plt.scatter(self.__x[:,0], pred, c='r')
        plt.figure()
        plt.title('Learning curve')
        plt.plot(range(self.__n_iterations), cost_history)
        print('coef_determination : ', self.coef_determination(pred))

linear_model = LinearModelMultiClass(x, y, 0.1, 500)
linear_model.init_model()
theta_final, cost_history = linear_model.gradient_descent()
predictions = linear_model.predictions(linear_model.get_X(), theta_final)

linear_model.evaluation(predictions)